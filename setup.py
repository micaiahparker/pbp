from setuptools import setup, find_packages

setup(
    author="Micaiah Parker",
    name="pbp",
    packages=find_packages(),
    entry_points = {
        'console_scripts': [
            'pbp=pbp.cli:cli'
        ]
    }   
)