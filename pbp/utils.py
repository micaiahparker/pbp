import os
from pathlib import Path
from dotenv import load_dotenv, find_dotenv

def get_pbp_path(path=None):
    if path:
        path = Path(path)
    else:
        path = Path(os.environ.get('PBP_GAME_PATH', Path.home().joinpath('.pbp')))
    if not path.exists():
        os.mkdir(path.absolute())
    return path
