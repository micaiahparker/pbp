import os
import click

def create_session(name, rules):
    if name.exists():
        raise FileExistsError(f"Session: {name} already exists")
    else:
        os.mkdir(name)
        with open(name.joinpath('rules.json'), 'wt') as fd:
            fd.write(rules.read())