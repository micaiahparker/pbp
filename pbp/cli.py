import os
import click
from .utils import get_pbp_path
from .session import create_session

@click.group()
@click.option('--debug/--no-debug', default=True)
@click.pass_context
def cli(ctx, debug):
    ctx.ensure_object(dict)
    ctx.obj['DEBUG'] = debug

@cli.command()
@click.option('--ignore-exists/--no-ignore-exists', default=False)
@click.argument('name')
@click.argument('rules', type=click.File('r'))
@click.pass_context
def init(ctx, ignore_exists, name, rules):
    try:
        create_session(get_pbp_path().joinpath(name), rules)
        click.echo(f"Succesfully created: {name}")
    except FileExistsError as e:
        if not ignore_exists:
            raise e
    
